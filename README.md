# Resolución del problema - Itinerario
Para la resolución del problema está basada en la teoría de grafos y en el algoritmo Dijkstra.

Se han implementado las siguientes tareas

* Tarea 1
* Tarea 3

Se puede acceder al código fuente mediante git en el siguiente enlace:

* https://bitbucket.org/dlupah/itinerario.git

### Tecnología
El lenguaje utilizado es Java versión jdk 1.7.0_60

Para compilar y ejecutar los test se ha utilizado Maven versión 3.0.5

para la creación de los test se han utilizado los siguientes frameworks:

* junit versión 4.10
* hamcrest versión 1.3

Las pruebas se han realizado en entorno Windows 8.1 64bits.

### Ejecución

Para ejecutar los test se ejecutará lo siguiente en la consola correspondiente del SO:

```sh
$ mvn test
```

### Desarrollador

David López Hernández

