package es.dixiland.itinerario;

/**
 * Interfaz que representa un grafo orientado a una compañia aérea con las distintas
 * operaciones que se pueden realizar sobre cálculo de itinerarios.
 * @author David López Hernández
 */
public interface Compania {

    /**
     * Agrega a la compañia un aeropuerto
     * @param codigo Codigo identificativo del aeropuerto
     * @param nombre nombre descriptivo del aeropuerto
     */
    void insertarAeropuerto(String codigo, String nombre);

    /**
     * Agrega a la compañia una ruta entre un origen y un destino.
     * @param codigoOrigen codigo identificativo del aeropuerto de origen
     * @param codigoDestino codigo identificativo del aeropuerto de destino
     * @param tiempoHoras numero de horas que se tarda desde el aeropuerto origen hasta el destino
     */
    void insertarRuta(String codigoOrigen, String codigoDestino, int tiempoHoras);
    
    /**
     * Devuelve las horas que se tarda en realizar un itinerario con 2 o mas puntos de paso
     * @param itinerarios tantos códigos de aeropuerto como puntos de paso tiene el itinerario
     * @return tiempo en horas que se tarda en el itinerario seleccionado. 
     *         Devuelve -1 si el codigo no es valido o no se puede realizar el itinerario.
     */
    int getTiempoVueloItinerario(String ... itinerarios);
    
    /**
     * Devuelve el número minimo de horas que se tarda en realizar un itinerario entre 
     * dos puntos dados. 
     * @param codigoOrigen codigo identificativo del aeropuerto de origen
     * @param codigoDestino codigo identificativo del aeropuerto de destino
     * @return tiempo en horas que se tarda en el itinerario seleccionado. 
     *         Devuelve -1 si el codigo no es valido o no se puede realizar el itinerario.

     */
    int getTiempoVueloItinerarioMasRapido(String codigoOrigen, String codigoDestino);
    
}
