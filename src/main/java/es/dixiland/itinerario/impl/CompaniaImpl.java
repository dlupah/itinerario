package es.dixiland.itinerario.impl;

import es.dixiland.itinerario.Compania;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementación del grafo de Compañia
 * @author David López Hernández
 */
public class CompaniaImpl implements Compania {

    private final Map<String, Aeropuerto> aeropuertos;

    /**
     * Constructor de la implementación de la Compañia
     */
    public CompaniaImpl() {
        aeropuertos = new HashMap<>();
    }
    
    /**
     * {@inheritDoc }
     */
    @Override
    public void insertarAeropuerto(String codigo, String nombre) {
        aeropuertos.put(codigo, new Aeropuerto(codigo, nombre));
    }
    
    /**
     * {@inheritDoc }
     */  
    @Override
    public void insertarRuta(String codigoOrigen, String codigoDestino, int tiempoHoras) {
        Aeropuerto aeropuertoOrigen = getAeropuertoConExcepcion(codigoOrigen);
        Aeropuerto aeropuertoDestino = getAeropuertoConExcepcion(codigoDestino);
        
        Ruta ruta = new Ruta(aeropuertoDestino, tiempoHoras);
        aeropuertoOrigen.getRutas().add(ruta);
    }
    
    /**
     * {@inheritDoc }
     */  
    @Override
    public int getTiempoVueloItinerario(String ... itinerarios) {
        if (itinerarios == null || itinerarios.length < 2) {
            return -1;
        }
        int tiempoVuelo = 0;
        Aeropuerto aeropuerto = aeropuertos.get(itinerarios[0]);
        if (aeropuerto == null)  {
            return -1;
        }
        for (int i = 1 ; i < itinerarios.length ; i++) {
            Ruta ruta = aeropuerto.getRutaAeropuertoAdyacente(itinerarios[i]);
            if (ruta == null) {
                return -1;
            }
            tiempoVuelo += ruta.getTiempoHoras();
            aeropuerto = ruta.getAeropuertoDestino();
        }
        return tiempoVuelo;
    }
    
    /**
     * {@inheritDoc }
     */
    @Override
    public int getTiempoVueloItinerarioMasRapido(String codigoOrigen, String codigoDestino) {
        Aeropuerto aeropuertoOrigen = aeropuertos.get(codigoOrigen);
        if (aeropuertoOrigen == null || !aeropuertos.containsKey(codigoDestino)) {
            return -1;
        }
        Map<String, Integer> distanciasMinimas = new HashMap<>();
        exploraAeropuerto(aeropuertoOrigen, 0, distanciasMinimas);
        Integer distanciaMimina = distanciasMinimas.get(codigoDestino);
        return distanciaMimina != null?distanciaMimina:-1;
    }
    
    /**
    * Explora un aeropuerto dado guardando la información relativa a la distancia mínima que hay respecto al primero de todos.
    * @param aeropuerto aeropuerto a explorar
    * @param distanciaAcumulada distancia acumulada hasta llegar al aeropuerto a explorar
    * @param distanciasMinimas mapa que contiene la relación entre un aeropuerto y el resto de aeropuertos con 
    *                          las distancias mímimas que hay para llegar
    */
    private void exploraAeropuerto(Aeropuerto aeropuerto, int distanciaAcumulada, Map<String, Integer> distanciasMinimas) {
        Integer distanciaMiminaExplorada = distanciasMinimas.get(aeropuerto.getCodigo());
        if (distanciaMiminaExplorada == null || distanciaAcumulada < distanciaMiminaExplorada || distanciaMiminaExplorada == 0) {
            distanciasMinimas.put(aeropuerto.getCodigo(), distanciaAcumulada);
            exploraAeropuertosAdyacentes(aeropuerto, distanciaAcumulada, distanciasMinimas);
        }
    }
    
    /**
    * Explora todos los aeropuertos adyacentes de un aeropuerto dado
    * @param aeropuerto aeropuerto padre que contiene los aeropuertos adyacentes
    * @param distanciaAcumulada distancia acumulada hasta llegar al aeropuerto a explorar
    * @param distanciasMinimas mapa que contiene la relación entre un aeropuerto y el resto de aeropuertos con 
    *                          las distancias mímimas que hay para llegar
    */
    private void exploraAeropuertosAdyacentes(Aeropuerto aeropuerto, int distanciaAcumulada, Map<String, Integer> distanciasMinimas) {
        for(Ruta ruta : aeropuerto.getRutas()) {
            exploraAeropuerto(ruta.getAeropuertoDestino(), distanciaAcumulada + ruta.getTiempoHoras(), distanciasMinimas);
        }   
    }
    
    /**
    * Recupera un aeropuerto a partir de su código lanzando un excepción si el aeropuerto no exixte
    * @param codigo código del aeropuerto
    * @return Aeropuerto correspondiente al código recibido 
    */
    private Aeropuerto getAeropuertoConExcepcion(String codigo) {
        Aeropuerto aeropuerto = aeropuertos.get(codigo);
        if (aeropuerto == null) {
            throw new ItinerarioException("No existe el aeropuerto " + codigo);
        }        
        return aeropuerto;
    }
    
    
}
