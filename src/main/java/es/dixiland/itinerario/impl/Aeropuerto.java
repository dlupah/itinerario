package es.dixiland.itinerario.impl;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que representa un vertice del grafo
 * @author David López Hernández
 */
public class Aeropuerto {
    private String codigo;
    private String nombre;
    private List<Ruta> rutas;

    /**
     * Constructor basado en un código de aeropuerto y su nombre
     * @param codigo código del aeropuerto
     * @param nombre nombre del aeropuerto
     */
    public Aeropuerto(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
        rutas = new ArrayList<>();
    }

    /**
     * Recupera la Ruta adyacente a partir del código del aeropuerto
     * @param codigo código del aeropuerto
     * @return Ruta asociada al aeropuerto
     */
    public Ruta getRutaAeropuertoAdyacente(String codigo) {
        for(Ruta ruta : rutas) {
            if (ruta.getAeropuertoDestino().codigo.equals(codigo)) {
                return ruta;
            }
        }
        return null;
    };

    /**
     * Recupera el Aeropuerto adyacente a partir del código del aeropuerto
     * @param codigo código del aeropuerto
     * @return Aeropuerto asociado al código
     */
    public Aeropuerto getAeropuertoAdyacente(String codigo) {
        Ruta ruta = getRutaAeropuertoAdyacente(codigo);
        return ruta!=null?ruta.getAeropuertoDestino():null;
    };
    
    /**
     * Devuelve el código del aeropuerto
     * @return código del aeropuerto
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Establece el código del aeropuerto
     * @param codigo código del aeropuerto
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Devuelve el nombre del aeropuerto
     * @return nombre del aeropuerto
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Establece el nombre del aeropuerto
     * @param nombre nombre del aeropuerto
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Devuelve la lista de rutas adyacentes
     * @return rutas adyacentes
     */
    public List<Ruta> getRutas() {
        return rutas;
    }

    /**
     * Establece la lista de rutas adyacentes
     * @param rutas rutas adyacentes
     */
    public void setRutas(List<Ruta> rutas) {
        this.rutas = rutas;
    }
}
