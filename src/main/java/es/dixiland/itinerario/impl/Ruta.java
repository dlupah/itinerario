package es.dixiland.itinerario.impl;

/**
 * Establece la relación con un aeropuerto de destino con el tiempo en horas que se tarda en llegar.
 * @author David López Hernández
 */
public class Ruta {
    private Aeropuerto aeropuertoDestino;
    private int tiempoHoras;

    /**
     * Constructor basado en el aeropuerto y el tiempo que se tarda en llegar
     * @param aeropuertoDestino aeropuerto de destino de la ruta
     * @param tiempoHoras tiempo que se tarda en llegar al aeropuerto destino
     */
    public Ruta(Aeropuerto aeropuertoDestino, int tiempoHoras) {
        this.aeropuertoDestino = aeropuertoDestino;
        this.tiempoHoras = tiempoHoras;
    }
    
    /**
     * Devuelve el aeropuerto de destino
     * @return aeropuerto de destino de la ruta
     */
    public Aeropuerto getAeropuertoDestino() {
        return aeropuertoDestino;
    }

    /**
     * Establece el aeropuerto de destino
     * @param aeropuertoDestino aeropuerto de destino de la ruta
     */
    public void setAeropuertoDestino(Aeropuerto aeropuertoDestino) {
        this.aeropuertoDestino = aeropuertoDestino;
    }

    /**
     * Devuelve el tiempo en horas
     * @return tiempo que se tarda en llegar al aeropuerto destino
     */
    public int getTiempoHoras() {
        return tiempoHoras;
    }

    /**
     * Establece el tiempo en horas
     * @param tiempoHoras tiempo que se tarda en llegar al aeropuerto destino
     */
    public void setTiempoHoras(int tiempoHoras) {
        this.tiempoHoras = tiempoHoras;
    }
    
}
