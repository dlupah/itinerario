package es.dixiland.itinerario.impl;

/**
 * Excepcion propia de la implementación declarada como no checked para que no se tenga que declarar al usar la interfaz
 * @author David López Hernández
 */
public class ItinerarioException extends RuntimeException {

    /**
     * Crea la excepción sin información añadida
     */
    public ItinerarioException() {
    }

    /**
     * Crea la excepción con un mensaje
     * @param message mensaje descriptivo del error
     */
    public ItinerarioException(String message) {
        super(message);
    }

    /**
     * Crea la excepción con un mensaje y el objeto que origina el error
     * @param message mensaje descriptivo del error
     * @param cause objeto que contiene el error que se ha provocado.
     */
    public ItinerarioException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Crea la excepción con el objeto que origina el error
     * @param cause objeto que contiene el error que se ha provocado.
     */
    public ItinerarioException(Throwable cause) {
        super(cause);
    }
    
}
