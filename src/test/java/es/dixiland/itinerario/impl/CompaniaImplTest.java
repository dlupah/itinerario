package es.dixiland.itinerario.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import org.junit.Test;
import org.junit.BeforeClass;
import es.dixiland.itinerario.utils.UtilFichero;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.*;

/**
 * Test de la implementación del grafo de la Compañia
 * @author David López Hernández
 */
public class CompaniaImplTest {

    private static CompaniaImpl compania;
    
    /**
     * Realiza una carga de datos de aeropuertos y rutas global para todos los test.
     */
    @BeforeClass
    public static void cargarDatos() throws IOException, URISyntaxException {
        compania = new CompaniaImpl();
        
        UtilFichero.cargaArchivoCSV("aeropuertos.csv", new UtilFichero.LeeLineas() {
            @Override public void trataDatos(String[] datos) {
                compania.insertarAeropuerto(datos[0], datos[1]);
            }
        });
        
        UtilFichero.cargaArchivoCSV("rutas.csv", new UtilFichero.LeeLineas() {
            @Override public void trataDatos(String[] datos) {
                compania.insertarRuta(datos[0], datos[1], Integer.valueOf(datos[2]));
            }
        });        
    }
    
    @Test
    public void testGetTiempoVueloItinerarioDebeDevolverElTiempoDeVueloCorrecto() throws IOException, URISyntaxException {
        UtilFichero.cargaArchivoCSV("tiempo_vuelo.csv", new UtilFichero.LeeLineas() {
            @Override public void trataDatos(String[] datos) {
                int tiempoVueloSupuesto = datos.length==2?Integer.valueOf(datos[1]):-1;
                int tiempoVuelo = compania.getTiempoVueloItinerario(datos[0].split("-"));
                assertThat("El tiempo de vuelo no es correcto", tiempoVuelo, is(equalTo(tiempoVueloSupuesto)));
            }
        });  
    }
    
    @Test
    public void testGetTiempoVueloItinerarioDebeDevolverMenosUnoCuandoSePasaMenosDeUnCodigoDeAeropuerto() throws IOException, URISyntaxException {
        int tiempoVuelo = compania.getTiempoVueloItinerario("M");
        assertThat("El tiempo de vuelo no es -1", tiempoVuelo, is(equalTo(-1)));
    }    
    
    @Test
    public void testGetTiempoVueloItinerarioDebeDevolverMenosUnoCuandoElPrimerParametroNoEsUnCodigoDeAeropuerto() throws IOException, URISyntaxException {
        int tiempoVuelo = compania.getTiempoVueloItinerario(null, "M");
        assertThat("El tiempo de vuelo no es -1", tiempoVuelo, is(equalTo(-1)));
        tiempoVuelo = compania.getTiempoVueloItinerario("T", "M");
        assertThat("El tiempo de vuelo no es -1", tiempoVuelo, is(equalTo(-1)));        
    }       
    
    @Test
    public void testGetTiempoVueloItinerarioMasRapidoDebeDevolverElTiempoDeVueloCorrecto() throws IOException, URISyntaxException {
        UtilFichero.cargaArchivoCSV("tiempo_vuelo_mas_rapido.csv", new UtilFichero.LeeLineas() {
            @Override public void trataDatos(String[] datos) {
                int tiempoVueloSupuesto = datos.length==3?Integer.valueOf(datos[2]):-1;
                int tiempoVuelo = compania.getTiempoVueloItinerarioMasRapido(datos[0], datos[1]);
                assertThat("El tiempo de vuelo más rápido no es correcto", tiempoVuelo, is(equalTo(tiempoVueloSupuesto)));
            }
        });  
    }
    
    @Test
    public void testGetTiempoVueloItinerarioMasRapidoDebeDevolverMenosUnoCuandoLosCodigosNoCoincidenConUnCodigoDeAeropuerto() throws IOException, URISyntaxException {
        int tiempoVuelo = compania.getTiempoVueloItinerarioMasRapido(null, "O");
        assertThat("El tiempo de vuelo más rápido no es -1", tiempoVuelo, is(equalTo(-1)));
        tiempoVuelo = compania.getTiempoVueloItinerarioMasRapido("T", "O");
        assertThat("El tiempo de vuelo más rápido no es -1", tiempoVuelo, is(equalTo(-1)));  
        tiempoVuelo = compania.getTiempoVueloItinerarioMasRapido("O", null);
        assertThat("El tiempo de vuelo más rápido no es -1", tiempoVuelo, is(equalTo(-1)));           
        tiempoVuelo = compania.getTiempoVueloItinerarioMasRapido("O", "T");
        assertThat("El tiempo de vuelo más rápido no es -1", tiempoVuelo, is(equalTo(-1)));           
    }    
    
}
