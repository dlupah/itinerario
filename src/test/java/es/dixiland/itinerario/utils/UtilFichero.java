package es.dixiland.itinerario.utils;

import es.dixiland.itinerario.impl.CompaniaImplTest;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Utilidades para tratar ficheros
 * @author David López Hernández
 */
public abstract class UtilFichero {

    /**
     * Interfaz para tratar lineas de datos separadas por punto y coma
     */
    public interface LeeLineas {
        public void trataDatos(String[] datos);
    }
    
    /**
    * Lee un fichero dado de tipo CSV con los datos separados por punto y coma y lo trata invocando 
    * al método LeeLineas#trataDatos por cada linea del fichero.
    * @param nombre nombre del archivo CSV
    * @param leeLineas objeto que recibirá cada una de las lineas del fichero en formato Array de String por 
    *                  cada posición encontrada de la separación de punto y coma.
    */
    public static void cargaArchivoCSV(String nombre, LeeLineas leeLineas) throws IOException, URISyntaxException {
        URL url = CompaniaImplTest.class.getResource("/" + nombre);
        Path resPath = Paths.get(url.toURI());
        Charset charset = Charset.forName("UTF-8");
        List<String> lineas = Files.readAllLines(resPath, charset);
        
        for (String linea : lineas) {
            leeLineas.trataDatos(linea.split(";"));
        }   
    }
    
}
